//
//  AppUITesting.swift
//  DiscontBankApp
//
//  Created by niv ben-porath on 15/05/2022.
//  Copyright © 2022 nbpApps. All rights reserved.
//

import Foundation

struct AppUITesting {
    static var isUITesting: Bool {
        return ProcessInfo.processInfo.environment[AppUITesting.LaunchEnvironmentKey.isUITesting] == AppUITesting.LaunchEnvironmentValue.isTrue
    }
            
//    static var shouldConsiderUserLoggedIn: Bool {
//        return ProcessInfo.processInfo.environment[AppUITesting.LaunchEnvironmentKey.shouldConsiderUserLoggedIn] == AppUITesting.LaunchEnvironmentValue.isTrue
//    }
//
//    static var shouldUseMockData: Bool {
//        return ProcessInfo.processInfo.environment[AppUITesting.LaunchEnvironmentKey.shouldUseMockData] == AppUITesting.LaunchEnvironmentValue.isTrue
//    }
//
//    static var shouldStartWithAtLeastOneItemInCart: Bool {
//        return ProcessInfo.processInfo.environment[AppUITesting.LaunchEnvironmentKey.shouldStartWithAtLeastOneItemInCart] == AppUITesting.LaunchEnvironmentValue.isTrue
//    }
//
//    static var mockDataSet: MockDataSet? {
//        guard let mockDataSetRawValue = ProcessInfo.processInfo.environment[AppUITesting.LaunchEnvironmentKey.mockDataSetRawValue],
//              let mockDataSet = MockDataSet(rawValue: mockDataSetRawValue) else {
//            return nil
//        }
//
//        return mockDataSet
//    }
}


extension AppUITesting {
    struct LaunchEnvironmentKey {
        static let isUITesting = "isUITesting"
        static let shouldConsiderUserLoggedIn = "shouldConsiderUserLoggedIn"
        static let shouldUseMockData = "shouldUseMockData"
        static let shouldStartWithAtLeastOneItemInCart = "shouldStartWithAtLeastOneItemInCart"
        static let mockDataSetRawValue = "mockDataSetRawValue"
    }
        
    struct LaunchEnvironmentValue {
        static let isTrue = "isTrue"
        static let isFalse = "isFalse"
    }
}

