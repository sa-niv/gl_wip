//
//  DiscontBankAppUITests.swift
//  DiscontBankAppUITests
//
//  Created by niv ben-porath on 21/03/2020.
//  Copyright © 2020 nbpApps. All rights reserved.
//

import XCTest

class DiscontBankAppUITests: XCTestCase {

    var app : XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // UI tests must launch the application that they test.
        

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
        }
    }
    
    func test_launchApp_withUserHasSeenOnboardingTapSetting_settingsShouldBeVisable() {
        XCTContext.runActivity(named: "Test Tap Settings") { _ in
            XCTContext.runActivity(named: "Open app with user that has seen onboarding") { _ in
                launchAppForUITesting_withUserPassedOnboarding()
            }
            
            XCTContext.runActivity(named: "Check for open order") { _ in
                subtest_launchApp_withUserThatPassedOnboarding_TappingSettings_shouldOpenSettingsPage()
            }
        }
    }
    
    func subtest_launchApp_withUserThatPassedOnboarding_TappingSettings_shouldOpenSettingsPage() {
        guard let app = app else {
            XCTAssert(false, "app has not been launched")
            return
        }
        
        app.buttons["Settings"].tap()
        XCTAssert(app.staticTexts["Settings"].exists)
    }
}


extension DiscontBankAppUITests {
    
    func launchAppForUITesting_withUserPassedOnboarding() {
        let launchEnvironment = [
            AppUITesting.LaunchEnvironmentKey.isUITesting: AppUITesting.LaunchEnvironmentValue.isTrue,
//            AppUITesting.LaunchEnvironmentKey.shouldUseMockData: AppUITesting.LaunchEnvironmentValue.isTrue,
//            AppUITesting.LaunchEnvironmentKey.shouldConsiderUserLoggedIn: AppUITesting.LaunchEnvironmentValue.isTrue,
        ]
        
        app = DiscontBankAppUITests.appLaunched(with: launchEnvironment)
    }
    
    private static func appLaunched(with launchEnvironment: [String: String]) -> XCUIApplication {
        let app = XCUIApplication()
        app.launchEnvironment = launchEnvironment
        app.launch()
        
        return app
    }
}

